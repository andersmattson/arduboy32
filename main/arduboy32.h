#include "Arduino.h"
#include <esp_log.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <freertos/timers.h>
#include "time.h"
#include "EEPROM.h"

TimerHandle_t timer;

void _loop(void *){
    loop();
}

extern "C" void app_main()
{
    initArduino();

    setup();
    timer=xTimerCreate("nes",configTICK_RATE_HZ/100, pdTRUE, NULL, _loop);
    xTimerStart(timer, 0);
 
}
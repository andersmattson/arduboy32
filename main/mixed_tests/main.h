
#define byte uint8_t

void drawLives();
void Score();

bool titleScreen();
bool displayHighScores(byte file);
void newLevel();
void drawPaddle();
void pause();
void drawBall();
void drawGameOver();
void enterHighScore(byte file);
void playTone(unsigned int frequency, unsigned int duration);
void playToneTimed(unsigned int frequency, unsigned int duration);

#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := arduboy32

COMPONENT_ADD_LDFLAGS=-lstdc++ -l$(COMPONENT_NAME)

include $(IDF_PATH)/make/project.mk


#include "Arduboy.h"
#include "audio.h"

bool ArduboyAudio::audio_enabled = false;

void ArduboyAudio::begin(){

};

void ArduboyAudio::on(){
    audio_enabled = true;
};

void ArduboyAudio::off(){
    audio_enabled = false;
};

void ArduboyAudio::saveOnOff(){

};

bool ArduboyAudio::enabled(){
    return audio_enabled;
};

void ArduboyTunes::initChannel(byte pin){

};

void ArduboyTunes::playScore(const byte *score){

};

void ArduboyTunes::stopScore(){

};

void ArduboyTunes::delay(unsigned msec){

};

void ArduboyTunes::closeChannels(){};

bool ArduboyTunes::playing(){
    return false;
};

void ArduboyTunes::tone(unsigned int frequency, unsigned long duration){

};

void static step(){};
void static soundOutput(){};


void static playNote (byte chan, byte note){};
void static stopNote (byte chan){};
